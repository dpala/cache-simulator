#! /bin/bash

pids=""
for size in 1024 2048 4096 8192 16384 32768 
do 
    fname=${1##*/}
    fname=${fname%.*}
    resdir="./res/cache$size"
    if [ ! -d $resdir ]; then
        mkdir $resdir 
    fi
    python3 src/cache_sim.py -f $1 -s $size -b 32  -o "$resdir/$fname.csv" &
    pids="$pids $!"
done

wait $pids
