import argparse
import numpy as np
import pandas as pd
from collections import defaultdict
from cache import Cache

def nvm_writes(flushes, evicts, words_in_block):
    return (flushes + evicts) * words_in_block

def parse_line(line):
    '''Parse a line of the csv file and returns cycle, address and is_store'''
    cycle, op, op_size, addr = line.split(',')
    cycle = int(cycle)
    addr = int(addr) & 0xffffffff
    is_store = op == 'ST'
    return cycle, addr, is_store

def main():
    parser = argparse.ArgumentParser(description='Cache simulator')
    parser.add_argument('-f', '--trace-file', dest='trace_file', required=True, type=argparse.FileType('r'))
    parser.add_argument('-o', '--out-file', dest='out_file', type=argparse.FileType('w'))
    parser.add_argument('-s', '--cache-size', dest='cache_size', type=int, default=1024)
    parser.add_argument('-a', '--associativity', dest='associativity', type=int, default=4)
    parser.add_argument('-b', '--block-size', dest='block_size', type=int, default=16)
    parser.add_argument('-i', '--interval', dest='interval', type=int, default=1e7)

    args = parser.parse_args()

    cache_size = args.cache_size
    associativity = args.associativity
    block_size = args.block_size

    cache = Cache(size=cache_size, associativity=associativity, block_size=block_size)
    words_in_block = cache.block_size // 4

    i = 0
    intervals = []
    #interval = defaultdict(int)
    interval = {'evicts': 0, 'hits': 0, 'misses': 0, 'stores': 0}

    with args.trace_file as trace_file:
        for line in trace_file:
            cycle, addr, is_store = parse_line(line) 
            resp = cache.access(addr, is_store)

            interval['evicts'] += resp.evict
            interval['hits'] += resp.hit
            interval['misses'] += not resp.hit
            interval['stores'] += is_store 

            if i != (cycle // args.interval):
                interval['flushes'] = cache.write_back()
                interval['mem_wr'] = nvm_writes(interval['flushes'], interval['evicts'], words_in_block)
                intervals.append(interval)
                i += 1
                #interval = defaultdict(int)  
                interval = {'evicts': 0, 'hits': 0, 'misses': 0, 'stores': 0}

    interval['flushes'] = cache.write_back()
    interval['mem_wr'] = nvm_writes(interval['flushes'], interval['evicts'], words_in_block)
    intervals.append(interval)

    df = pd.DataFrame(intervals)
    df.index.name = 'interval'

    if args.out_file:
        df.to_csv(args.out_file) 
    else:
        print(df)

if __name__ == '__main__':
    main()
