import numpy as np
import pandas as pd
from collections import namedtuple

Response = namedtuple('Response', ['hit', 'evict'])

class Cache:
    ''' Simulate the tag and control of a Write-Back data cache '''

    def __init__(self, size=1024, block_size=16, associativity=4):
        self.size = size
        self.block_size = block_size
        self.associativity = associativity
        self.num_lines = size // (block_size * associativity)
        self.index = int(np.log2(self.num_lines))
        self.block_sel =  int(np.log2(self.block_size // 4))
        
        self.tags = [[0 for _ in range(associativity)] for _ in range(self.num_lines)]
        self.valids = [[False for _ in range(associativity)] for _ in range(self.num_lines)]
        self.dirty = [[False for _ in range(associativity)] for _ in range(self.num_lines)]

        np.random.seed(0)

    def __repr__(self):
        cols = [f'set_{i}' for i in range(self.associativity)]
        tag_df = pd.DataFrame(self.tags, columns=['tag_'+c for c in cols]) 
        valid_df = pd.DataFrame(self.valids, columns=['valid_'+c for c in cols])
        dirty_df = pd.DataFrame(self.dirty, columns=['dirty_'+c for c in cols])
        return repr(pd.concat([tag_df, valid_df, dirty_df], axis=1))

    def parse_addr(self, addr):
        '''Computes tag and line number (index) from an address'''
        word_addr = (addr >> 2) & 0xffffffff
        tag = word_addr >> (self.index + self.block_sel)         
        line_n = (word_addr >> self.block_sel) % self.num_lines 
        return line_n, tag 


    def write_back(self):
        '''Clears the dirty and valid bits and returns the number of dirty bits'''
        count_dirty = sum([sum(d) for d in self.dirty]) 

        for l in range(self.num_lines):
            for s in range(self.associativity):
                self.valids[l][s] = False
                self.dirty[l][s] = False
        return count_dirty

    def access(self, addr, is_store):
        '''Access the cache at the given address and returns a Response tuple (hit,evict)'''
        line_n, tag = self.parse_addr(addr)

        if tag in self.tags[line_n]:
            set_idx = self.tags[line_n].index(tag)
            valid = self.valids[line_n][set_idx]
            if valid:
                self.dirty[line_n][set_idx] |= is_store # dirty if store else stay the same
                return Response(hit=True, evict=False)

        if all(self.valids[line_n]): 
            set_idx = np.random.randint(0, self.associativity) 
            evict = True
        else:
            set_idx = self.valids[line_n].index(False) # Take first available place
            evict = False

        self.tags[line_n][set_idx] = tag
        self.valids[line_n][set_idx] = True
        self.dirty[line_n][set_idx] = is_store
        return Response(hit=False, evict=evict)

    def read(self, addr):
        return self.access(addr, False)

    def write(self, addr):
        return self.access(addr, True)
