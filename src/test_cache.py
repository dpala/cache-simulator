from cache import Cache

def small_test(cache_size=1024, associativity=4, block_size=16):
    c = Cache(size=cache_size, associativity=associativity, block_size=block_size)
    print(c)
    print(f'read addr: 1; {c.read(1)}')
    print(c)
    print(f'read addr: 2; {c.read(2)}')
    print(f'read addr: 3; {c.read(3)}')
    print(f'read addr: 4; {c.read(4)}')

    print(c)
    print(f'read addr: 40; {c.read(40)}')
    print(f'read addr: 41; {c.read(41)}')
    print(f'read addr: 42; {c.read(42)}')
    print(f'read addr: 43; {c.read(43)}')
    print(c)

    print(f'write addr: 43; {c.write(43)}')
    print(f'write addr: 41; {c.write(41)}')
    print(f'write addr: 17; {c.write(17)}')
    print(f'write addr: 4; {c.write(4)}')

    print(c)

    print(f'write back: {c.write_back()} blocks')
    
    print(c)
    print(f'read addr: 0; {c.read(0)}')
    print(f'read addr: 1; {c.read(1)}')
    print(f'read addr: 2; {c.read(2)}')
    print(f'read addr: 3; {c.read(3)}')
    print(c)
    print(f'read addr: 100; {c.read(100)}')
    print(f'read addr: 101; {c.read(101)}')
    print(f'read addr: 102; {c.read(102)}')
    print(f'read addr: 103; {c.read(103)}')

if __name__ == '__main__':
    small_test()
